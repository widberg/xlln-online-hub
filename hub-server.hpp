#pragma once

namespace IS_CORE_SOCKET {
	enum Type : uint8_t {
		UNKNOWN = 0,
		YES,
		NO,
	};
}

struct REMOTE_SOCKET_INFO {
	sockaddr_storage sockaddr;
	time_t lastComm;
	uint32_t instanceId = 0;
	IS_CORE_SOCKET::Type isCoreSocket = IS_CORE_SOCKET::Type::UNKNOWN;
	uint32_t xllnVersion = 0;
	uint32_t titleId = 0;
	uint32_t titleVersion = 0;
	uint16_t portBaseHBO = 0;
	
	std::map<uint8_t, sockaddr_storage> *portInternalOffsetToExternalAddr = 0;
	std::map<uint16_t, sockaddr_storage> *portInternalOriginalToExternalAddr = 0;
};

extern sockaddr_storage xlln_sockaddr_base_0;
extern pthread_mutex_t xlln_mutex_exit_threads;
extern pthread_mutex_t xlln_mutex_packet_consume;
extern std::vector<REMOTE_SOCKET_INFO*> xlln_remote_socket_info;

int InitHubServer();
int UninitHubServer();

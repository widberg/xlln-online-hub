#pragma once

bool FloatIsNaN(float &vagueFloat);
bool rfc3986_allow(char i);
char* encode_rfc3986(char* label_literal, int label_literal_length);
int TrimRemoveConsecutiveSpaces(char* text);
void PadCStringWithChar(char* strToPad, int toFullLength, char c);
char CmpVersions(char* version_base, char* version_alt);
void wcstombs2(char* buffer, wchar_t* text, int buf_len);
const char* LastPathItem(const char *pathname);
const char* LastPathItem(const char *pathname, size_t end_length);
const char* FilenameFromPathname(const char *pathname);
char* PathFromFilename(const char *filename_path);
size_t LenOfFirstPathItem(const char *pathname);
size_t StrSameChrFor(const char *primary, const char *secondary);
size_t StrSamePathFor(const char *primary, const char *secondary);
char* GetItemBeforeLastPos(const char *full_pathname, const char *&end_position);
char* GetRealPath(const char *path);
bool EndsWith(const char *str, const char *suffix);
char* CloneString(const char *str);
char* FormMallocString(const char *const format, ...);
uint8_t* Sha256StrToByteArray(const char *checksumStr);
char* Sha256ByteArrayToStr(const uint8_t *checksum);
uint16_t GetSockAddrPort(const sockaddr_storage *sockAddrStorage);
bool SetSockAddrPort(sockaddr_storage *sockAddrStorage, uint16_t portHBO);
char* GetSockAddrInfo(const sockaddr_storage *sockAddrStorage);
bool SockAddrsMatch(const sockaddr_storage *sockAddr1, const sockaddr_storage *sockAddr2);

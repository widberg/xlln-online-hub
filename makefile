xlln-hub: main.o sha256.o utils.o stats.o hub-server.o
	g++ -Wall -pedantic -pthread -g3 -O0 -o bin/xlln-hub bin/main.o bin/sha256.o bin/utils.o bin/stats.o bin/hub-server.o
unittests: tests.o sha256.o utils.o
	g++ -Wall -pedantic -g3 -O0 -o bin/unittests bin/tests.o bin/sha256.o bin/utils.o
clean:
	# Delete files in "bin/" with no filename extension.
	find ./bin/ ! -iname "*.*" -type f -exec rm {} +
	# Delete files in "bin/" whose name ends with with ".o".
	find ./bin/ -name "*.o" -type f -exec rm {} +
	# Delete files in "bin/" whose name ends with with ".s".
	find ./bin/ -name "*.s" -type f -exec rm {} +
	# Delete files in "bin/" whose name ends with with ".ii".
	find ./bin/ -name "*.ii" -type f -exec rm {} +
main.o: main.cpp
	g++ -g3 -O0 -save-temps=obj -c main.cpp -o bin/main.o
tests.o: tests.cpp
	g++ -g3 -O0 -save-temps=obj -c tests.cpp -o bin/tests.o
sha256.o: sha256.cpp
	g++ -g3 -O0 -save-temps=obj -c sha256.cpp -o bin/sha256.o
utils.o: utils.cpp
	g++ -g3 -O0 -save-temps=obj -c utils.cpp -o bin/utils.o
stats.o: stats.cpp
	g++ -g3 -O0 -save-temps=obj -c stats.cpp -o bin/stats.o
hub-server.o: hub-server.cpp
	g++ -g3 -O0 -save-temps=obj -c hub-server.cpp -o bin/hub-server.o

# -O0 Reduce compile time and make debugging produce expected results.
# -O3 Most Optimised.

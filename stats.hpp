#pragma once
#include <map>

extern FILE *stats_file;

void GetStatsConnectedEntities(size_t *numOfConnectedInstances, size_t *numOfAdditionalSockets, size_t *numOfRecentlyIgnoredUnknownAddrs);
void GetStatsTitleVersions(std::map<uint64_t, size_t>& versions);
void GetStatsXllnVersion(std::map<uint32_t, size_t>& versions);
void* ThreadStatsFile(void *arg);
int InitStats();
int UninitStats();
